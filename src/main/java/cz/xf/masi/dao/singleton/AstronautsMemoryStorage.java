package cz.xf.masi.dao.singleton;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import cz.xf.masi.dao.AstronautDao;
import cz.xf.masi.models.Astronaut;

@Component
public class AstronautsMemoryStorage implements AstronautDao {
	private List<Astronaut> astronauts = new ArrayList<>();

	@Override
	public boolean addAstronaut(final Astronaut astronaut) {
		Astronaut newAstronaut = new Astronaut(astronaut);
		newAstronaut.setId(getLastAstronautId() + 1);
		astronauts.add(newAstronaut);
		return true;
	}

	@Override
	public boolean deteleAstronaut(final int id) {
		Astronaut astronaut = findAstronautById(id);
		if (astronaut == null) { 
			return false;
		} else {
			astronauts.remove(astronaut);
			return true;
		}
	}

	@Override
	public boolean updateAstronaut(final Astronaut astronaut) {
		Astronaut newAstronaut = findAstronautById(astronaut.getId());
		
		if (newAstronaut == null) return false;
		
		newAstronaut.setFirstName(astronaut.getFirstName());
		newAstronaut.setLastName(astronaut.getLastName());
		newAstronaut.setBirthdayDate(astronaut.getBirthdayDate());
		newAstronaut.setSuperpowers(astronaut.getSuperpowers());
		return true;
	}

	@Override
	public Astronaut findAstronautById(final int id) {
		for (Astronaut astronaut : astronauts) {
			if (astronaut.getId() == id) return astronaut;
		}
		return null;
	}
	
	@Override
	public List<Astronaut> getAllAstronauts() {
		return astronauts;
	}

	/**
	 * Get last inserted ID of an astronaut.
	 * @return last ID.
	 */
	private int getLastAstronautId() {
		if (astronauts.isEmpty()) {
			return 0;
		} else {
			Astronaut astronaut = astronauts.get(astronauts.size() - 1);
			return astronaut.getId();
		}
	}
	
	
}
