package cz.xf.masi.dao;

import java.util.List;

import cz.xf.masi.models.Astronaut;

public interface AstronautDao {
	boolean addAstronaut(Astronaut astronaut);
	boolean deteleAstronaut(int id);
	boolean updateAstronaut(Astronaut astronaut);
	Astronaut findAstronautById(int id);
	List<Astronaut> getAllAstronauts();
}
