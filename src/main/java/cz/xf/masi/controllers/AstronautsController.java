package cz.xf.masi.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.xf.masi.dao.AstronautDao;
import cz.xf.masi.models.Astronaut;
import cz.xf.masi.propertyEditors.SuperpowersPropertyEditor;

@Controller
public class AstronautsController {
	@Autowired
	private AstronautDao astronautDao;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(List.class, new SuperpowersPropertyEditor());
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String astronautsList(Model model) {
		model.addAttribute("astronauts", astronautDao.getAllAstronauts());
		model.addAttribute("title", "Seznam astronautů");
		return "astronautList";
	}
	
	@RequestMapping(value = "/novyKosmonaut", method = RequestMethod.GET)
	public String showAddAstronautForm(Model model) {
		model.addAttribute("astronaut", new Astronaut());
		model.addAttribute("title", "Přidat kosmonauta");
		return "astronautAdd";
	}
	
	@RequestMapping(value = "/novyKosmonaut", method = RequestMethod.POST) 
	public String addAstronaut(@Valid Astronaut astronaut, BindingResult bindingResult, RedirectAttributes redirectAttributes){
		if (bindingResult.hasErrors()) {
			return "astronautAdd";
		}
		
		boolean status = astronautDao.addAstronaut(astronaut);
		redirectAttributes.addFlashAttribute("saved", status);
		return "redirect:/";
	}
	
	@RequestMapping(value = "/upravitKosmonauta/{id}", method = RequestMethod.GET)
	public String showEditAstronautForm(Model model, @PathVariable int id, RedirectAttributes redirectAttributes) {
		Astronaut astronaut = astronautDao.findAstronautById(id);
		if (astronaut == null) {
			redirectAttributes.addFlashAttribute("found", false);
			return "redirect:/";
		}
		
		model.addAttribute("astronaut", astronaut);
		model.addAttribute("title", "Upravit kosmonauta");
		return "astronautEdit";
	}
	
	@RequestMapping(value = "/upravitKosmonauta/{id}", method = RequestMethod.POST)
	public String editAstronaut(Model model, @PathVariable int id, @Valid Astronaut astronaut, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		Astronaut originalAstronaut = astronautDao.findAstronautById(astronaut.getId());
		if (originalAstronaut == null) {
			redirectAttributes.addFlashAttribute("found", false);
			return "redirect:/";
		}
		
		if (bindingResult.hasErrors()) {
			return "astronautEdit";
		}
		
		boolean status = astronautDao.updateAstronaut(astronaut);
		
		redirectAttributes.addFlashAttribute("updated", status);
		return "redirect:/";
	}
	
	@RequestMapping(value = "/smazatKosmonauta/{id}", method = RequestMethod.GET)
	public String deleteAstronaut(@PathVariable int id, RedirectAttributes redirectAttributes) {
		boolean status = astronautDao.deteleAstronaut(id);
		redirectAttributes.addFlashAttribute("removed", status);
		return "redirect:/";
	}
}
