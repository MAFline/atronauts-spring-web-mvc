package cz.xf.masi.propertyEditors;

import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SuperpowersPropertyEditor extends PropertyEditorSupport {

	@Override
	public String getAsText() {
		List<String> superpowers = (List<String>) this.getValue();
		
		if (superpowers == null) return "";
		
		StringBuilder superpowersList = new StringBuilder();
		for (String superpower : superpowers) {
			superpowersList.append(superpower).append(",");
		}
		
		return superpowersList.substring(0, superpowersList.length() - 1);
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		List<String> superpowers = new ArrayList<>(Arrays.asList(text.split(",")));
		this.setValue(superpowers);
	}

	
	
}
