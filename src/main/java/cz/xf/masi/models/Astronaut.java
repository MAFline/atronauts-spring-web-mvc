package cz.xf.masi.models;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

public class Astronaut {
	private int id;
	
	@NotBlank(message = "Vyplňte prosím hodnotu.")
	private String firstName;
	
	@NotBlank(message = "Vyplňte prosím hodnotu.")
	private String lastName;
	
	@DateTimeFormat(pattern = "dd. MM. yyyy")
	@NotNull(message = "Zadejte datum ve formátu \"dd. MM. yyyy\".")
	private Date birthdayDate;
	
	@NotEmpty(message = "Zadejte alespoň jednu superschopnost.")
	private List<String> superpowers;
	
	public Astronaut() {
	}

	public Astronaut(String firstName, String lastName, Date birthdayDate, List<String> superpowers) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdayDate = birthdayDate;
		this.superpowers = superpowers;
	}
	
	public Astronaut(Astronaut astronaut) {
		this.firstName = astronaut.getFirstName();
		this.lastName = astronaut.getLastName();
		this.birthdayDate = astronaut.getBirthdayDate();
		this.superpowers = astronaut.getSuperpowers();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthdayDate() {
		return birthdayDate;
	}

	public void setBirthdayDate(Date birthdayDate) {
		this.birthdayDate = birthdayDate;
	}

	public List<String> getSuperpowers() {
		return superpowers;
	}

	public void setSuperpowers(List<String> superpowers) {
		this.superpowers = superpowers;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (!(o instanceof Astronaut)) return false;
		
		Astronaut astronaut = (Astronaut) o;
		if (!this.firstName.equals(astronaut.getFirstName())) return false;
		if (!this.lastName.equals(astronaut.getLastName())) return false;
		if (!this.birthdayDate.equals(astronaut.getBirthdayDate())) return false;
		if (!this.superpowers.equals(astronaut.getSuperpowers())) return false;
		
		return true;
	}

	@Override
	public int hashCode() {
		return this.firstName.length() * this.lastName.length();
	}

}
