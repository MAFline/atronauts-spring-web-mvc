<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">

<c:if test="${empty title}">
	<title>TechFides Astronauts</title>
</c:if>
<c:if test="${!empty title}">
	<title>${title}</title>
</c:if>
</head>

<body>
	<div class="container">

		<c:if test="${saved == true}">
			<div class="alert alert-success" role="alert">Nový kosmonaut
				přidán.</div>
		</c:if>

		<c:if test="${removed == true}">
			<div class="alert alert-success" role="alert">Kosmonaut úspěšně
				vymazán.</div>
		</c:if>

		<c:if test="${removed == false}">
			<div class="alert alert-danger" role="alert">Žádný kosmonaut
				nevymazán.</div>
		</c:if>

		<c:if test="${found == false}">
			<div class="alert alert-danger" role="alert">Požadovaný
				kosmonaut neexistuje.</div>
		</c:if>

		<c:if test="${updated == true}">
			<div class="alert alert-success" role="alert">Kosmonaut byl
				upraven.</div>
		</c:if>

		<h1>Seznam kosmonautů</h1>

		<a href="${pageContext.request.contextPath}/novyKosmonaut"
			class="btn btn-primary btn-lg active" role="button"><strong>Přidat
				kosmonauta</strong></a> <br /> <br />

		<c:if test="${empty astronauts}">
			<div class="alert alert-info" role="alert">
				Nebyl nalezen <strong>žádný</strong> kosmonaut.
			</div>
		</c:if>

		<c:if test="${!empty astronauts}">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Jméno</th>
						<th>Příjmení</th>
						<th>Datum narození</th>
						<th>Superschopnosti</th>
						<th>Akce</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="astronaut" items="${astronauts}">
						<tr>
							<td class="text-left"><c:out value="${astronaut.id}" /></td>
							<td class="text-left"><c:out value="${astronaut.firstName}" /></td>
							<td class="text-left"><c:out value="${astronaut.lastName}" /></td>
							<td class="text-left"><fmt:formatDate
									value="${astronaut.birthdayDate}" pattern="dd. MM. yyyy" /></td>
							<td class="text-left"><c:forEach var="superpower"
									items="${astronaut.superpowers}" varStatus="loop">
									<c:out value="${superpower}" /><c:if test="${!loop.last}">, </c:if>
								</c:forEach></td>
							<td class="text-left"><a type="button"
								class="btn btn-warning btn-xs"
								href="${pageContext.request.contextPath}/upravitKosmonauta/${astronaut.id}">upravit</a>
								<a type="button" class="btn btn-warning btn-xs"
								href="${pageContext.request.contextPath}/smazatKosmonauta/${astronaut.id}">smazat</a></td>
					</c:forEach>
				</tbody>
			</table>
		</c:if>

	</div>
</body>

</html>