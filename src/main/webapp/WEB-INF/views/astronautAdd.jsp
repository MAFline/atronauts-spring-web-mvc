<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">

<style>
.astronaut-form {
	max-width: 650px;
}
</style>

<c:if test="${empty title}">
	<title>TechFides Astronauts</title>
</c:if>
<c:if test="${!empty title}">
	<title>${title}</title>
</c:if>
</head>

<body>
	<div class="container">

		<h1>Nový kosmonaut</h1>

		<form:form class="form-horizontal astronaut-form" role="form"
			method="post"
			action="${pageContext.request.contextPath}/novyKosmonaut"
			commandName="astronaut">
			<div class="form-group">
				<label for="firstName" class="col-sm-3 control-label">Jméno:
				</label>
				<div class="col-sm-9">
					<form:input class="form-control" id="firstName" name="firstName"
						path="firstName" type="text" />
				</div>
				<form:errors path="firstName"
					cssClass="col-sm-offset-3 col-sm-9 form-errors"></form:errors>
			</div>

			<div class="form-group">
				<label for="lastName" class="col-sm-3 control-label">Příjmení:
				</label>
				<div class="col-sm-9">
					<form:input class="form-control" id="lastName" name="lastName"
						path="lastName" type="text" />
				</div>
				<form:errors path="lastName"
					cssClass="col-sm-offset-3 col-sm-9 form-errors"></form:errors>
			</div>
			
			<div class="form-group">
				<label for="birthdayDate" class="col-sm-3 control-label">Datum narození:
				</label>
				<div class="col-sm-9">
					<form:input class="form-control" id="birthdayDate" name="birthdayDate"
						path="birthdayDate" type="text" />
				</div>
				<form:errors path="birthdayDate"
					cssClass="col-sm-offset-3 col-sm-9 form-errors"></form:errors>
			</div>
			
			<div class="form-group">
				<label for="superpowers" class="col-sm-3 control-label">Superschopnosti:
				</label>
				<div class="col-sm-9">
					<form:input class="form-control" id="superpowers" name="superpowers"
						path="superpowers" type="text" />
				</div>
				<form:errors path="superpowers"
					cssClass="col-sm-offset-3 col-sm-9 form-errors"></form:errors>
			</div>

			<div class="form-group">
				<span class="col-sm-1 col-sm-offset-1"> <a
					class="btn btn-primary"
					href="${pageContext.request.contextPath}/">Zpět</a>
				</span> <span class="col-sm-1 col-sm-offset-9">
					<button type="submit" class="btn btn-primary pull-right">Přidat</button>
				</span>
			</div>

		</form:form>

	</div>
</body>

</html>